import { LocalStorage } from 'quasar'

export default ({ router }) => {
  router.beforeEach((to, from, next) => {
    const logged = LocalStorage.getItem('loggedIn')
    if (!logged && to.path !== '/auth') {
       next('/auth')
    } else {
      next()
    }
  })
}
