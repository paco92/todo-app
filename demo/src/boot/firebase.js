import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

var firebaseConfig = {
  apiKey: 'AIzaSyBu-YerQ0Vy9mnRaEEQlh2P-IbXft3ID0A',
  authDomain: 'todolist-32a14.firebaseapp.com',
  databaseURL: 'https://todolist-32a14.firebaseio.com',
  projectId: 'todolist-32a14',
  storageBucket: 'todolist-32a14.appspot.com',
  messagingSenderId: '264646091934',
  appId: '1:264646091934:web:6e9cf16cdc10a6883c878f',
  measurementId: 'G-G3ET0V41GG'
}
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig)

const firebaseAuth = firebaseApp.auth()
const firebaseDb = firebase.database()
export {
  firebaseAuth,
  firebaseDb
}
