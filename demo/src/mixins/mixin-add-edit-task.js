import ModalHeader from 'components/Tasks/Modals/Shared/Modal-header.vue'
import ModalTaskName from 'components/Tasks/Modals/Shared/ModalTaskName.vue'
import ModalTaskDueDate from 'components/Tasks/Modals/Shared/ModalTaskDueDate.vue'
import ModalTaskDueTime from 'components/Tasks/Modals/Shared/ModalTaskDueTime.vue'
import ModalButtons from 'components/Tasks/Modals/Shared/ModalButtons.vue'

export default {
    components: {
        'modal-header': ModalHeader,
        'modal-task-name': ModalTaskName,
        'modal-task-due-date': ModalTaskDueDate,
        'modal-task-due-time': ModalTaskDueTime,
        'modal-buttons': ModalButtons
    }
}
