import { firebaseAuth } from 'boot/firebase'
import { LocalStorage, Loading } from 'quasar'
import { showErrorMessage } from 'src/functions/function-show-error-message'
const state = {
    logged: false
}

const mutations = {
    setLoggedIn (state, value) {
        state.logged = value
    }
}

const actions = {
    registerUser ({ commit }, payload) {
        Loading.show()
        firebaseAuth.createUserWithEmailAndPassword(payload.email, payload.password)
        .then(response => {
        }).catch(error => {
            showErrorMessage(error.message)
        })
    },
    loginUser ({ commit }, payload) {
        Loading.show()
        firebaseAuth.signInWithEmailAndPassword(payload.email, payload.password)
        .then(response => {
        })
        .catch(error => {
            showErrorMessage(error.message)
        })
    },
    logoutUser () {
        firebaseAuth.signOut()
    },
    handleAuth ({ commit, dispatch }) {
       firebaseAuth.onAuthStateChanged((user) => {
           Loading.hide()
            if (user) {
                commit('setLoggedIn', true)
                LocalStorage.set('loggedIn', true)
                if (this.$router.path === '/') {
                    this.$router.push('/')
                }
                dispatch('tasks/fbReadData', null, { root: true })
            } else {
                commit('setLoggedIn', false)
                LocalStorage.set('loggedIn', false)
                this.$router.replace('/auth')
            }
       })
    }
}

const getters = {
    getLogged: (state) => {
        return state.logged
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}
