import { LocalStorage } from 'quasar'

const state = {
    settings: {
        hourTimeFormat: false,
        showOneList: false
    }
}

const mutations = {
    setTimeFormat (state, value) {
        state.settings.hourTimeFormat = value
    },
    setShowOneList (state, value) {
        state.settings.showOneList = value
    },
    setSettings (state, settings) {
        state.settings = settings
    }
}

const actions = {
    setTimeFormat ({ commit, dispatch }, value) {
        commit('setTimeFormat', value)
        dispatch('saveSettings')
    },
    setShowOneList ({ commit, dispatch }, value) {
        commit('setShowOneList', value)
        dispatch('saveSettings')
    },
    saveSettings ({ state }) {
        LocalStorage.set('settings', state.settings)
    },
    getSettings ({ commit }) {
        const settings = LocalStorage.getItem('settings')
        if (settings) {
            commit('setSettings', settings)
        }
    }
}

const getters = {
    getHourTimeFormat: (state) => {
        return state.settings.hourTimeFormat
    },
    getShowOneList: (state) => {
        return state.settings.showOneList
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}
