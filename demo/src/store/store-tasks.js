import Vue from 'vue'
import { uid } from 'quasar'
import { firebaseDb, firebaseAuth } from 'boot/firebase'

const state = {
    tasks: {
    },
    search: '',
    sort: 'name'
}

const mutations = {
    updateTask (state, payload) {
        Object.assign(state.tasks[payload.id], payload.updates)
    },
    deleteTask (state, id) {
        Vue.delete(state.tasks, id)
    },
    addTask (state, payload) {
        Vue.set(state.tasks, payload.id, payload.task)
    },
    setSearchValue (state, value) {
        state.search = value
    },
    setSortOption (state, value) {
        state.sort = value
    }
}

const actions = {
    updateTask ({ dispatch }, id) {
        dispatch('fbEditTask', id)
    },
    deleteTask ({ dispatch }, id) {
        dispatch('fbDeleteTask', id)
    },
    addTask ({ dispatch }, tasks) {
        const payload = {
            id: uid(),
            name: tasks.name,
            completed: false,
            dueDate: tasks.dueDate,
            dueTime: tasks.dueTime
        }

        dispatch('fbAddTask', payload)
    },
    fbAddTask ({ commit }, payload) {
        console.log('payload', payload)
        const currentUser = firebaseAuth.currentUser.uid
        const taskRef = firebaseDb.ref('tasks/' + currentUser + '/' + payload.id)

        payload.task = {
            name: payload.name,
            completed: payload.completed,
            dueDate: payload.dueDate,
            dueTime: payload.dueTime
        }
        taskRef.set(payload.task)
    },
    fbEditTask ({ commit }, payload) {
        const currentUser = firebaseAuth.currentUser.uid
        const taskRef = firebaseDb.ref('tasks/' + currentUser + '/' + payload.id)
        console.log('payload', payload)
        taskRef.update(payload.updates)
    },
    fbDeleteTask ({ commit }, id) {
        const currentUser = firebaseAuth.currentUser.uid
        const taskRef = firebaseDb.ref('tasks/' + currentUser + '/' + id)
        taskRef.remove()
    },
    setSearchValue ({ commit }, value) {
        commit('setSearchValue', value)
    },
    setSortOption ({ commit }, value) {
        commit('setSortOption', value)
    },
    fbReadData ({ commit }) {
        const currentUser = firebaseAuth.currentUser.uid
        const userTasks = firebaseDb.ref('tasks/' + currentUser)
        // child added
        userTasks.on('child_added', snapshot => {
            const task = snapshot.val()

            const payload = {
                id: snapshot.key,
                task: task
            }

            commit('addTask', payload)
        })

        // child changed
        userTasks.on('child_changed', snapshot => {
            const task = snapshot.val()

            const payload = {
                id: snapshot.key,
                updates: task
            }

            commit('updateTask', payload)
        })

        // child remove
        userTasks.on('child_removed', snapshot => {
            commit('deleteTask', snapshot.key)
        })
    }
}

const getters = {
    tasksToDo: (state, getters) => {
        const tasksFiltered = getters.taskFiltered
        const tasks = {}
        Object.keys(tasksFiltered).forEach(function (key) {
            const task = tasksFiltered[key]
            if (!task.completed) {
                tasks[key] = task
            }
        })
        return tasks
    },
    tasksCompleted: (state, getters) => {
        const tasksFiltered = getters.taskFiltered
        const tasks = {}
        Object.keys(tasksFiltered).forEach(function (key) {
            const task = tasksFiltered[key]

            if (task.completed) {
                tasks[key] = task
            }
        })

        return tasks
    },
    taskFiltered: (state, getters) => {
        const tasksSorted = getters.taskSorted
        const filtered = {}
        const search = state.search.toLowerCase()
        if (state.search.length) {
            Object.keys(tasksSorted).forEach(function (key) {
                const name = tasksSorted[key].name.toLowerCase()
                if (name.includes(search)) {
                    filtered[key] = tasksSorted[key]
                }
            })

            return filtered
        }

        return tasksSorted
    },
    taskSorted: (state) => {
        const tasksSorted = {}
        const keysOrdered = Object.keys(state.tasks)

        keysOrdered.sort((a, b) => {
            const taskAProp = state.tasks[a][state.sort].toLowerCase()
            const taskBProp = state.tasks[b][state.sort].toLowerCase()

            if (taskAProp > taskBProp) {
                return 1
            } else if (taskAProp < taskBProp) {
                return -1
            } else {
                return 0
            }
        })

        keysOrdered.forEach((key) => {
            tasksSorted[key] = state.tasks[key]
        })

        return tasksSorted
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}
